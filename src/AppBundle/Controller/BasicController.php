<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Comment;
use AppBundle\Entity\Photo;
use AppBundle\Entity\User;
use AppBundle\Form\CommentType;
use AppBundle\Form\PhotoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class BasicController extends Controller
{

    /**
     * @Route("/", name="homepage")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function homepageAction(Request $request){

            $users= $this->getDoctrine()
                ->getRepository('AppBundle:User')
                ->findAll();

            $publication_user = $this->getDoctrine()
                ->getRepository('AppBundle:Photo')
                ->findAll();

            return $this->render(':Homepage:homepage.html.twig', array(
                'users' => $users,
                'photos' => $publication_user,
                'user' => $this->getUser(),
            ));
    }
    /**
     * @Route("/new_post")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newPostAction(Request $request)
    {
        $photo = new Photo();
        $form = $this->createForm(PhotoType::class, $photo);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $photo->setUser($this->getUser());
            $photo->setDateTime(new\DateTime());
            $em->persist($photo);
            $em->flush();
            $user = $this->getUser();
            return $this->redirectToRoute('app_basic_profile', array(
               'id' =>  $user->getId(),
            ));
        }

        return $this->render('Content/post.html.twig', array(
            'form_post' => $form->createView(),
            'user' => $this->getUser(),
        ));
    }

    /**
     * @Route("/user/{id}", requirements={"id": "\d+"})
     * @Method({"GET", "POST"})
     * @param Request $request, $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function profileAction(Request $request, $id)
    {
        $publication_user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($id);
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($id);
        $remove_form = $this->createFormBuilder()
            ->add('remove', SubmitType::class, array('label' => 'Remove'))
            ->getForm();

        return $this->render('Content/profile.html.twig', array(
            'photos' => $publication_user->getPhotos(),
            'Photo_remove_form' => $remove_form,
            'user' => $user,
        ));
    }
    /**
     * @Route("/remove/photo/{id}", requirements={"id": "\d+"})
     * @Method({"GET", "POST"})
     * @param  $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function removePhotoAction(int $id)
    {
        $photo = $this->getDoctrine()
            ->getRepository('AppBundle:Photo')
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($photo);
        $em->flush();

        return $this->redirectToRoute('app_basic_profile',array(
            'id' => $photo->getId()
        ));
    }

    /**
     * @Route("/details/photo/{id}", requirements={"id": "\d+"})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function detailsPhotoAction(Request $request, $id)
    {

        $scores = [];
        $avg_score = null;

        $photo = $this->getDoctrine()
            ->getRepository('AppBundle:Photo')
            ->find($id);

        $comments = $this->getDoctrine()
            ->getRepository('AppBundle:Comment')
            ->findBy(['photo' => $id]);

        $comment = new Comment();
        $comment_form = $this->createForm(CommentType::class, $comment);

        foreach ($comments as $comment){
            $scores[] = $comment->getScore();
        }

        if (count($scores) > 0) {
            $avg_score = array_sum($scores) / count($scores);
        }
        return $this->render('Content/details.html.twig', array(
            'photo' => $photo,
            'comment_form' => $comment_form->createView(),
            'comments' => $photo->getComments(),
            'scores' => $avg_score,
            'user' => $this->getUser(),
        ));
    }

    /**
     * @Route("/add_comment/{id}", requirements={"id": "\d+"})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addCommentAction(Request $request, $id)
    {

        $photo = $this->getDoctrine()
            ->getRepository('AppBundle:Photo')
            ->find($id);

        $comment = new Comment();
        $comment_form = $this->createForm(CommentType::class, $comment);
        $comment_form->handleRequest($request);

        if ($comment_form->isSubmitted() && $comment_form->isValid()) {

            $comment->setPhoto($photo);
            $comment->setUser($this->getUser());
            $comment->setDatetime(new\DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
        }

        return $this->redirectToRoute('app_basic_detailsphoto',['id' => $photo->getId()]);

    }

}
