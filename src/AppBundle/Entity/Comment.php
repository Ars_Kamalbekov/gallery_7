<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Comment
 *
 * @ORM\Table(name="comment")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommentRepository")
 */
class Comment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="description", type="text")
     */
    protected $description;

    /**
     * @var integer
     * @Assert\NotBlank()
     * @ORM\Column(name="score", type="integer")
     */
    protected $score;

    protected $avg_score;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Photo",
     *     inversedBy="comment"
     * )
     * @ORM\JoinColumn(name="photo_id", referencedColumnName="id")
     */
    protected $photo;
    /**
     * @ORM\ManyToOne(
     *     targetEntity="User",
     *     inversedBy="comments"
     * )
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    protected $dateTime;

    public function __toString()
    {
        return $this->description ?: '';
    }

    /**
     * Add score
     *
     * @param $score
     */
    public function addScore( $score)
    {
        $this->avg_score += $score;
    }

    /**
     * @return mixed
     */
    public function getScores()
    {
        return $this->avg_score;
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Comment
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set photo
     *
     * @param Photo|null $user
     * @return Comment
     * @internal param Photo $photo
     *
     */
    public function setPhoto(\AppBundle\Entity\Photo $user = null)
    {
        $this->photo = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }

    /**
     * @param mixed $dateTime
     */
    public function setDateTime($dateTime)
    {
        $this->dateTime = $dateTime;
    }

    /**
     * @return mixed
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param int $score
     */
    public function setScore(int $score)
    {
        $this->score = $score;
    }



    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Comment
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
