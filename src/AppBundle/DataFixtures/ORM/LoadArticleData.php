<?php
/**
 * Created by PhpStorm.
 * User: arsen
 * Date: 22.07.17
 * Time: 16:07
 */
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Photo;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadArticleData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $user_admin = new User();
        $user_admin
            ->setUsername('Admin_01')
            ->setName('Admin')
            ->setSurname('Adminovich')
            ->setEmail('original@some.com')
            ->setEnabled(true)
            ->setRoles(['ROLE_ADMIN']);
        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user_admin, '123321');
        $user_admin->setPassword($password);
        $manager->persist($user_admin);

        $user_1 = new User();
        $user_1
            ->setUsername('qwe')
            ->setName('qwe')
            ->setSurname('qwe')
            ->setEmail('qwe@qwe.qwe')
            ->setEnabled(true);
        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user_1, 'qwe');
        $user_1->setPassword($password);
        $manager->persist($user_1);

        $user_2 = new User();
        $user_2
            ->setUsername('asd')
            ->setName('asd')
            ->setSurname('asd')
            ->setEmail('asd@asd.asd')
            ->setEnabled(true);
        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user_2, 'asd');
        $user_2->setPassword($password);
        $manager->persist($user_2);
//
//        $photo_1 = new Photo();
//        $photo_1
//            ->setPhotoFile('for_fixture')
//            ->setUser($user_1)
//            ->setDescription('описание к 1 фотке')
//            ->setComments(
//                'коммент к 1 фотке'
//            );
//        $manager->persist($photo_1);
//
//        $photo_2 = new Photo();
//        $photo_2
//            ->setPhotoFile('for_fixture_1')
//            ->setUser($user_1)
//            ->setDescription('описание к 2 фотке')
//            ->setComments('коммент к 2 фотке');
//        $manager->persist($photo_2);
//
//        $comment = new Comment();
//        $comment
//            ->setDescription('бла бла бла')
//            ->setUser($user_1)
//            ->setPhoto($photo_1)
//            ->setDateTime(new\DateTime());
//        $manager->persist($comment);

        $manager->flush();
    }
}
